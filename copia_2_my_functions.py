import numpy as np
import pandas as pd
from scipy import stats

def raggruppa(test, yhat):

    # aggiungo colonna delle previsioni al test set fornito
    test_with_yhat = pd.concat([test, pd.Series(yhat)], axis=1)

    # rinomino colonna delle y
    test_with_yhat.columns = ['PUBBLICITA', 'INIZIO_PUBBLICITA',
                              'FINE_PUBBLICITA', 'MACRO_TIPO_PUBBLICITA',
                              'FASCIA_ORARIA', 'FASCIA_TEMPISTICA',
                              'TIPO_PUBBLICITA', 'DATA',
                              'CODICE_GIOCO', 'RILANCIO_PUBBLICITA',
                              'SESSIONE', 'OFFERTA_PROMOZIONALE',
                              'MACRO_CATEGORIA_GIOCO', 'MICRO_CATEGORIA_GIOCO',
                              'AREA_CLICK', 'NUMERO_PUBBLICITA_CONCORRENTI',
                              'ID', 'NUMERO_CLICK_SITO']

    # dizionario "altre variabili" : "funzioni"
    dict_agg = {'INIZIO_PUBBLICITA': np.max,
                'FINE_PUBBLICITA': np.min,
                'SESSIONE': np.median,
                'NUMERO_PUBBLICITA_CONCORRENTI': np.mean,
                'ID': np.max,
                'NUMERO_CLICK_SITO': np.sum,
                'TIPO_PUBBLICITA': np.median,
                'OFFERTA_PROMOZIONALE': np.mean
                }

    # raggruppamento sul train dataset
    df_grouped = test_with_yhat.groupby(['PUBBLICITA', 'CODICE_GIOCO', 'MACRO_TIPO_PUBBLICITA',
                                         'FASCIA_TEMPISTICA', 'FASCIA_ORARIA', 'TIPO_PUBBLICITA',
                                         'MICRO_CATEGORIA_GIOCO', 'RILANCIO_PUBBLICITA', 'AREA_CLICK', 'DATA'])

    df_grouped = df_grouped.agg(dict_agg)

    return df_grouped


def crea_file_submission(df, number):

    df = pd.concat([df['ID'], df['NUMERO_CLICK_SITO']], axis=1)

    df['ID'] = df['ID'].astype('str')
    df['ID'].head()
    df['NUMERO_CLICK_SITO'] = df['NUMERO_CLICK_SITO'].astype('int')
    df.columns = ['ID', 'yhat']

    file = open("submission_{}.txt".format(number), "w")
    for index, row in df.iterrows():
        file.write(str(row['ID']) + " " + str(row['yhat']) + "\n")
    file.close()
    return None
