import numpy as np
import pandas as pd
from dateutil.parser import parse


def raggruppa(test, yhat):

    # aggiungo colonna delle previsioni al test set fornito
    test_with_yhat = pd.concat([test, pd.Series(yhat)], axis=1)

    # rinomino colonna delle y
    test_with_yhat.columns = ['PUBBLICITA', 'INIZIO_PUBBLICITA',
                              'FINE_PUBBLICITA', 'MACRO_TIPO_PUBBLICITA',
                              'FASCIA_ORARIA', 'FASCIA_TEMPISTICA',
                              'TIPO_PUBBLICITA', 'DATA',
                              'CODICE_GIOCO', 'RILANCIO_PUBBLICITA',
                              'SESSIONE', 'OFFERTA_PROMOZIONALE',
                              'MACRO_CATEGORIA_GIOCO', 'MICRO_CATEGORIA_GIOCO',
                              'AREA_CLICK', 'NUMERO_PUBBLICITA_CONCORRENTI',
                              'ID', 'NUMERO_CLICK_SITO']

    # dizionario "altre variabili" : "funzioni"
    dict_agg = {'INIZIO_PUBBLICITA': np.max,
                'FINE_PUBBLICITA': np.min,
                'SESSIONE': np.median,
                'NUMERO_PUBBLICITA_CONCORRENTI': np.mean,
                'ID': np.max,
                'NUMERO_CLICK_SITO': np.sum,
                'TIPO_PUBBLICITA': np.median,
                'OFFERTA_PROMOZIONALE': np.mean
                }

    # raggruppamento sul train dataset
    df_grouped = test_with_yhat.groupby(['PUBBLICITA', 'CODICE_GIOCO', 'MACRO_TIPO_PUBBLICITA',
                                         'FASCIA_TEMPISTICA', 'FASCIA_ORARIA', 'TIPO_PUBBLICITA',
                                         'MICRO_CATEGORIA_GIOCO', 'RILANCIO_PUBBLICITA', 'AREA_CLICK', 'DATA'])

    df_grouped = df_grouped.agg(dict_agg)
    return df_grouped


def crea_file_submission(df, number):

    df = pd.concat([df['ID'], df['NUMERO_CLICK_SITO']], axis=1)

    df['ID'] = df['ID'].astype('str')
    df['ID'].head()
    df['NUMERO_CLICK_SITO'] = df['NUMERO_CLICK_SITO'].astype('int')
    df.columns = ['ID', 'yhat']

    file = open("submission_{}.txt".format(number), "w")
    for index, row in df.iterrows():
        file.write(str(row['ID']) + " " + str(row['yhat']) + "\n")
    file.close()
    return None


def fix_vars(df):

    # Aggiustamento dtype delle variabili

    df['PUBBLICITA'] = df['PUBBLICITA'].astype('int').astype('category')
    df['MACRO_TIPO_PUBBLICITA'] = df['MACRO_TIPO_PUBBLICITA'].astype('category')
    df['FASCIA_ORARIA'] = df['FASCIA_ORARIA'].astype('category')
    df['FASCIA_TEMPISTICA'] = df['FASCIA_TEMPISTICA'].astype('category')
    df['CODICE_GIOCO'] = df['CODICE_GIOCO'].astype('category')
    df['RILANCIO_PUBBLICITA'] = df['RILANCIO_PUBBLICITA'].astype('int')
    df['SESSIONE'] = df['SESSIONE'].astype('int').astype('category')
    df['MACRO_CATEGORIA_GIOCO'] = df['MACRO_CATEGORIA_GIOCO'].astype('category')
    df['MICRO_CATEGORIA_GIOCO'] = df['MICRO_CATEGORIA_GIOCO'].astype('category')
    df['AREA_CLICK'] = df['AREA_CLICK'].astype('int')
    df['ID'] = df['ID'].astype('int').astype('category')
    df['INIZIO_PUBBLICITA'] = df['INIZIO_PUBBLICITA'].apply(parse)
    df['FINE_PUBBLICITA'] = df['FINE_PUBBLICITA'].apply(parse)
    df['DATA'] = df['DATA'].apply(parse)

    return df


def fix_inizio_fine(df):

    # Aggiustamento variabili INIZIO_PUBBLICTA e FINE_PUBBLICITA

    for i, _ in df.iterrows():
        if df['INIZIO_PUBBLICITA'][i] > df['FINE_PUBBLICITA'][i]:
            flag = df['INIZIO_PUBBLICITA'][i]
            df.loc[i, 'INIZIO_PUBBLICITA'] = df['FINE_PUBBLICITA'][i]
            df.loc[i, 'FINE_PUBBLICITA'] = flag
    return df


def fix_data_limit(df):

    # Aggiustamento var DATA per rispettare i limiti delle var INIZIO_PUBBLICITA e FINE_PUBBLICITA

    for i, _ in df.iterrows():

        if df['DATA'][i] < df['INIZIO_PUBBLICITA'][i]:
            flag_inizio = df['INIZIO_PUBBLICITA'][i]
            df.loc[i, 'INIZIO_PUBBLICITA'] = df['DATA'][i]
            df.loc[i, 'DATA'] = flag_inizio

        if df['DATA'][i] > df['FINE_PUBBLICITA'][i]:
            flag_fine = df['FINE_PUBBLICITA'][i]
            df.loc[i, 'FINE_PUBBLICITA'] = df['DATA'][i]
            df.loc[i, 'DATA'] = flag_fine
    return df


def insert_new_domenica(df):

    # inserimento nuova variabile NEW_DOMENICA

    # vettore giorni 2016
    days_2016 = pd.date_range("2016-01-01", periods=366, freq='d')

    # vettore dummy domenica
    dummy = [0, 0, 1]
    dummy.extend([0, 0, 0, 0, 0, 0, 1] * 52)
    dummy_domenica = dummy[:-1]

    # aggiungo primo dell'anno e vigilia di natale
    dummy_domenica[0] = 1
    dummy_domenica[-8] = 1

    # vettore domenica 2016
    domenica_2016 = []
    for i in range(len(dummy_domenica)):
        if dummy_domenica[i]:
            domenica_2016.append(days_2016[i])

    # aggiungo NEW_DOMENICA al training e al test set
    df['NEW_DOMENICA'] = 0
    for i, _ in df.iterrows():
        if df['DATA'][i] in domenica_2016:
            df.loc[i, 'NEW_DOMENICA'] = 1

    # modifico il tipo in categorica categorica
    df['NEW_DOMENICA'] = df['NEW_DOMENICA'].astype('category')
    return df


def insert_new_range(df):

    # range inizio - fine pubblicita

    df['NEW_RANGE'] = 0
    for i, _ in df.iterrows():
        df.loc[i, 'NEW_RANGE'] = pd.Timedelta(df['FINE_PUBBLICITA'][i] - df['INIZIO_PUBBLICITA'][i]).days
    df['NEW_RANGE'] = df['NEW_RANGE'].astype('int')
    return df


def score_fun(y_pred, y_true):
    return np.sqrt(np.mean((np.log(y_pred + 1) - np.log(y_true + 1))**2))
